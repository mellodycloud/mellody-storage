package mellody_storage

import "testing"

func TestNewStorage(t *testing.T) {
	t.Log("Try create new Azure storage provider...")

	azureProvider := NewStorage(Azure, getConfiguration())
	checkProvider(&azureProvider, t)

	t.Log("Try create unsupported engine...")

	defer func() {
		if r := recover(); r != nil {
			t.Logf("Recovery after attempt create storage engine was success. Error: %v.", r)
		}
	}()

	unsupportedProvider := NewStorage(Yandex, getConfiguration())
	checkProvider(&unsupportedProvider, t)
}

func checkProvider(provider *StorageProvider, t *testing.T) {
	if provider == nil {
		t.Errorf("Storage provider is nil. It's wrong behavior.")
	}
}