package mellody_storage

import (
	"testing"
	"io/ioutil"
)

func TestYandexStorageProvider_Create_Delete_Container(t *testing.T) {
	t.Log("Try to create container...")

	containerName := "mellody"
	yandexProvider := NewStorage(Yandex, getConfiguration())

	if err := yandexProvider.CreateContainer(containerName); err != nil {
		t.Error(err)
	}

	if err := yandexProvider.DeleteContainer(containerName); err != nil {
		t.Error(err)
	}
}

func TestYandexStorageProvider_ExistsContainer(t *testing.T) {
	t.Log("Try to check if container is exists")

	containerName := "mellody"
	yandexProvider := NewStorage(Yandex, getConfiguration())

	t.Log("- Prepare container")
	prepareYandexContainerBeforeTest(containerName, yandexProvider)

	defer removeYandexContainerAfterTest(containerName, yandexProvider)
	defer t.Log("- Remove container")

	if ok, err := yandexProvider.ExistsContainer(containerName); err != nil {
		t.Error(err)
	} else if !ok {
		t.Errorf("Container '%v' doesn't exist", containerName)
	}
}

func TestYandexStorageProvider_Get(t *testing.T) {
	t.Log("Try to get directory info")

	containerName := "mellody"
	yandexProvider := NewStorage(Yandex, getConfiguration())

	t.Log("- Prepare container")
	prepareYandexContainerBeforeTest(containerName, yandexProvider)

	defer removeYandexContainerAfterTest(containerName, yandexProvider)
	defer t.Log("- Remove container")

	if directoryInfo, err := yandexProvider.Get(containerName); err != nil {
		t.Error(err)
	} else if directoryInfo.Name() != containerName {
		t.Error("Expected struct with name '%v', but was got %v", containerName, directoryInfo)
	}
}

func TestYandexStorageProvider_List(t *testing.T) {
	t.Log("Try to get list of files")

	yandexProvider := NewStorage(Yandex, getConfiguration())
	if _, err := yandexProvider.List(); err != nil {
		t.Error(err)
	}
}

func TestYandexStorageProvider_GetUrl(t *testing.T) {
	t.Log("Try to get directory info")

	containerName := "mellody"
	yandexProvider := NewStorage(Yandex, getConfiguration())

	t.Log("- Prepare container")
	prepareYandexContainerBeforeTest(containerName, yandexProvider)

	defer removeYandexContainerAfterTest(containerName, yandexProvider)
	defer t.Log("- Remove container")

	if url := yandexProvider.GetUrl(containerName); len(url) == 0 {
		t.Error("Expected is not empty url")
	}
}

func TestYandexStorageProvider_Upload_Download_Delete(t *testing.T) {
	t.Log("Try to upload file to disk")

	fileName := "notice"
	yandexProvider := NewStorage(Yandex, getConfiguration())

	content, err := ioutil.ReadFile("test_data\\notice")
	if err != nil {
		panic(err)
	}

	if err := yandexProvider.Upload(fileName, content); err != nil {
		t.Error(err)
		return
	}

	defer yandexProvider.Delete(fileName)

	t.Log("Try to download file from disk")

	if content, err := yandexProvider.Download(fileName); err != nil {
		t.Error(err)
	} else if len(content) != 4 {
		t.Errorf("Expected 4 bytes, but go %v bytes", len(content))
	}
}

func prepareYandexContainerBeforeTest(containerName string, yandexProvider StorageProvider) {
	if err := yandexProvider.CreateContainer(containerName); err != nil {
		panic(err)
	}
}

func removeYandexContainerAfterTest(containerName string, yandexProvider StorageProvider) {
	if err := yandexProvider.DeleteContainer(containerName); err != nil {
		panic(err)
	}
}