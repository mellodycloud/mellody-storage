package mellody_storage

import (
	"fmt"
)

type CloudStorageListType string

const (
	Azure CloudStorageListType = "azure"
	Yandex CloudStorageListType = "yandex"
	Google CloudStorageListType = "google"
	Vkontakte CloudStorageListType = "vkontakte"
	DropBox CloudStorageListType = "dropbox"
)

// StorageProvider contains basic operation for to work with the cloud disk.
type StorageProvider interface {
	CreateContainer(name string) error
	DeleteContainer(name string) error
	ExistsContainer(name string) (bool, error)
	List() ([]FileInformer, error)
	Get(path string) (FileInformer, error)
	GetUrl(path string) string
	Download(path string) ([]byte, error)
	Upload(path string, content []byte) error
	Delete(path string) (bool, error)
	Exists(path string) (bool, error)
}

// DefaultFileInfo describes simple file.
type DefaultFileInfo struct {
	name      string
	size      int64
	hash      string
	url       string
}

func (fi *DefaultFileInfo) Name() string {
	return fi.name
}

func (fi *DefaultFileInfo) Size() int64 {
	return fi.size
}

func (fi *DefaultFileInfo) Path() string {
	panic("Not implemented")
}

func (fi *DefaultFileInfo) Hash() string {
	return fi.hash
}

func (fi *DefaultFileInfo) PublicUrl() string {
	return fi.url
}

// FileCloudExplorer retrieves information from a file in the cloud.
type FileInformer interface {
	Size() int64
	Name() string
	Path() string
	Hash() string
	PublicUrl() string
}

// Factory returns instance StorageProvider by name.
// Generate panic if you will try to create engine which is not supported.
func NewStorage(storageName CloudStorageListType, configuration map[string]string) StorageProvider {
	switch storageName {
	case Azure:
		if azureClient, err := NewAzureClient(configuration); err != nil {
			panic(fmt.Sprintf("Initialize AzureProvider was failed. %v", err))
		} else {
			return azureClient
		}
	case Yandex:
		if yandexClient, err := NewYandexStorage(configuration); err != nil {
			panic(fmt.Sprintf("Initialize YandexProvider was failed. %v", err))
		} else {
			return yandexClient
		}
	}

	panic(fmt.Sprintf("The '%v' unsupported storage engine", storageName))
}

