package mellody_storage

import (
	"testing"
	"io/ioutil"
)

func TestAzureStorageProvider_CreateDeleteContainer(t *testing.T) {
	t.Log("Try to create container and then delete it...")

	containerName := "000containertestname"
	azureProvider := NewStorage(Azure, getConfiguration())

	if err := azureProvider.CreateContainer(containerName); err != nil {
		t.Error(err)
	}

	if err := azureProvider.DeleteContainer(containerName); err != nil {
		t.Error(err)
	}
}

func TestAzureStorageProvider_ExistsContainer(t *testing.T) {
	t.Log("Try to check that container is exists...")

	configuration := getConfiguration()
	azureProvider := NewStorage(Azure, configuration)
	if _, err := azureProvider.ExistsContainer(configuration[AZURE_CONTAINER_NAME]); err != nil {
		t.Error(err)
	}
}

func TestAzureStorageProvider_List(t *testing.T) {
	t.Log("Try to retrieves all files from store...\n")

	azureProvider := NewStorage(Azure, getConfiguration())

	_, err := azureProvider.List()
	if err != nil {
		t.Errorf("Retrieves data %v", err)
		return
	}
}

func TestAzureStorageProvider_Upload_Download(t *testing.T) {
	t.Log("Try to upload file on server\n")

	azureProvider := NewStorage(Azure, getConfiguration())

	content, err := ioutil.ReadFile("test_data\\notice")
	if err != nil {
		panic(err)
	}

	err = azureProvider.Upload("notice", content)
	if err != nil {
		panic(err)
	}

	t.Log("Try to download file from server\n")

	content, err = azureProvider.Download("notice")
	if err != nil {
		panic(err)
	}

	// Check that we downloaded four bytes.
	if len(content) != 4 {
		t.Errorf("Expected 4 bytes, but gave %v bytes", len(content))
	}
}

func TestAzureStorageProvider_Exists(t *testing.T) {
	t.Log("Try to check if file is exists.\n")

	azureProvider := NewStorage(Azure, getConfiguration())

	_, err := azureProvider.Exists("notice")
	if err != nil {
		t.Error(err)
	}
}

func TestAzureStorageProvider_GetUrl(t *testing.T) {
	t.Log("Try to get url to file download...")

	azureProvider := NewStorage(Azure, getConfiguration())

	url := azureProvider.GetUrl("notice")
	if url == "" {
		t.Error("Got error, expected non empty url.")
	}

	expectedUrl := "https://mellodyblob.blob.core.windows.net/user1/notice"
	if url != expectedUrl {
		t.Errorf("Expected '%v', but was received '%v'", "12", expectedUrl)
	}
}

func TestAzureStorageProvider_Delete(t *testing.T) {
	t.Log("Try to delete blob from container...")

	azureProvider := NewStorage(Azure, getConfiguration())

	content := []byte{48, 48, 48, 48}
	filename := "notice_for_delete"
	if err := azureProvider.Upload(filename, content); err != nil {
		t.Error("Error while try to upload file to server...")
		return
	}

	if result, err := azureProvider.Delete(filename); err != nil {
		t.Error("Occurred error while try to delete blob")
	}else if !result {
		t.Error("File wasn't deleted because file don't exists.")
	}
}