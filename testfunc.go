package mellody_storage

import "os"

//
// Notes: This file contains helper functions for tests.


// getConfiguration returns map with settings for creating an instance AzureProvider.
func getConfiguration() map[string]string {
	configure := make(map[string]string)
	configure[AZURE_CONTAINER_NAME] = os.Getenv(AZURE_CONTAINER_NAME)
	configure[AZURE_ACCOUNT_NAME] = os.Getenv(AZURE_ACCOUNT_NAME)
	configure[AZURE_ACCOUNT_KEY] = os.Getenv(AZURE_ACCOUNT_KEY)

	configure[YANDEX_OAUTH_TOKEN] = os.Getenv(YANDEX_OAUTH_TOKEN)

	return configure
}