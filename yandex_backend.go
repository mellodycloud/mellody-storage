package mellody_storage

import (
	"github.com/go-resty/resty"
	"fmt"
	"encoding/json"
)

const (
	YANDEX_OAUTH_TOKEN = "ya-oauth-token"
	YANDEX_APP_ID = "ya-application-id"
	YANDEX_APP_PASSWORD = "ya-application-password"

	YANDEX_API_BASE_URL = "https://cloud-api.yandex.net/v1"
)


// YandexStorageProvider implements StorageProvider interface for Yandex Disk
type YandexStorageProvider struct {
	Token string
}

// YandexFileInfo implements FileInformer interface. It stores information yandex's file about.
type YandexFileInfo struct {
	*DefaultFileInfo
}

// ResourceGetResponse is response on request GET /v1/disk/resources
type ResourceGetResponse struct {
	Size       int64           `json:"size"`
	PublicKey  string        `json:"public_key"`
	PublicUrl  string        `json:"public_url"`
	Name       string        `json:"name"`
	ResourceId string        `json:"resource_id"`
	Deleted    string        `json:"deleted"`
	Type       string        `json:"type"`
	MimeType   string        `json:"mime_type"`
	Hash       string        `json:"hash"`
}

// response: GET /v1/disk/resources/files
type ResourceListGetResponse struct {
	Items *[]ResourceGetResponse        `json:"items"`
}

// response: GET /v1/disk/resources/download and GET /v1/disk/resources/upload
type LinkResponse struct {
	Url       string        `json:"href"`
	Method    string        `json:"method"`
	Templated bool        `json:"templated"`
}

func NewYandexStorage(configuration map[string]string) (*YandexStorageProvider, error) {
	token, isTokenKeyExists := configuration[YANDEX_OAUTH_TOKEN]
	if !isTokenKeyExists {
		return nil, raiseErrorKeyDoesNotFound(YANDEX_OAUTH_TOKEN)
	}

	return &YandexStorageProvider{Token: token}, nil
}

func (provider *YandexStorageProvider) CreateContainer(name string) error {
	return provider.put("disk/resources", map[string]string{
		"path": name,
	})
}

func (provider *YandexStorageProvider) DeleteContainer(name string) error {
	_, err := provider.Delete(name)
	return err
}

func (provider *YandexStorageProvider) ExistsContainer(name string) (bool, error) {
	return provider.Exists(name)
}

func (provider *YandexStorageProvider) Get(path string) (FileInformer, error) {
	responseAsBytes, err := provider.get("disk/resources", map[string]string{"path": path})
	if err != nil {
		return nil, err
	}

	resource := &ResourceGetResponse{}
	if err := json.Unmarshal(responseAsBytes, resource); err != nil {
		return nil, err
	}

	fileInfo := converResourceGetResponseToFileInfo(resource)
	return fileInfo, nil
}

func (provider *YandexStorageProvider) GetUrl(path string) string {
	if link, err := getUrl("disk/resources/download", path, provider); err != nil {
		return ""
	} else {
		return link.Url
	}
}

func (provider *YandexStorageProvider) List() ([]FileInformer, error) {
	responseAsBytes, err := provider.get("disk/resources/files", make(map[string]string))
	if err != nil {
		return nil, err
	}

	resourceList := &ResourceListGetResponse{}
	if err := json.Unmarshal(responseAsBytes, resourceList); err != nil {
		return nil, err
	}

	items := *resourceList.Items
	files := make([]FileInformer, len(items))
	for idx, item := range items {
		files[idx] = converResourceGetResponseToFileInfo(&item)
	}

	return files, nil
}

func (provider *YandexStorageProvider) Download(path string) ([]byte, error) {
	url := provider.GetUrl(path)

	response, err := resty.R().Get(url)

	if httpError := check(response, err); httpError != nil {
		return nil, httpError
	}

	return response.Body(), nil
}

func (provider *YandexStorageProvider) Upload(path string, content []byte) error {
	//1. Gets link where we should upload file
	link, err := getUrl("disk/resources/upload", path, provider)
	if err != nil {
		return err
	}

	// 2. Uploads file from link
	return provider.upload(link.Url, content)
}

func (provider *YandexStorageProvider) Exists(path string) (bool, error) {
	_, err := provider.Get(path)
	return err == nil, err
}

func (provider *YandexStorageProvider) Delete(path string) (bool, error) {
	err := provider.delete("disk/resources", map[string]string{
		"path": path,
	})

	return err == nil, err
}

//
// REST extensions

func (provider *YandexStorageProvider) get(url string, params map[string]string) ([]byte, error) {
	resp, err := provider.rest().SetQueryParams(params).Get(makeUrl(url))

	if httpError := check(resp, err); httpError != nil {
		return nil, httpError
	}

	responseAsBytes := []byte(resp.String())
	return responseAsBytes, nil
}

func (provider *YandexStorageProvider) put(url string, params map[string]string) error {
	resp, err := provider.rest().SetQueryParams(params).Put(makeUrl(url))

	return httpErrorOrNil(resp, err)
}

func (provider *YandexStorageProvider) delete(url string, params map[string]string) error {
	resp, err := provider.rest().SetQueryParams(params).Delete(makeUrl(url))

	return httpErrorOrNil(resp, err)
}

func (provider *YandexStorageProvider) upload(url string, content []byte) error {
	resp, err := resty.R().
	SetBody(content).
	SetContentLength(true).
	Put(url)

	return check(resp, err)
}

//
// Helper functions


func (provider *YandexStorageProvider) rest() *resty.Request {
	return resty.R().
	SetHeader("Accept", "application/json").
	SetHeader("Authorization", fmt.Sprintf("OAuth %v", provider.Token))
}

// url makes address to request data. Address contains from BASE_URL + VERSION + <RESOURCE_NAME>.
func makeUrl(resource string) string {
	return fmt.Sprintf("%v/%v", YANDEX_API_BASE_URL, resource)
}

// Simple method, either retrieves error if exists or returns nil.
func httpErrorOrNil(resp *resty.Response, err error) error {
	if httpResponseErr := check(resp, err); httpResponseErr != nil {
		return httpResponseErr
	}

	return nil
}

// checks http status code and returns specific errors.
func check(response *resty.Response, err error) error {
	if err != nil {
		return &UnsupportedError{InnerError: &err}
	}

	switch response.StatusCode(){
	case 200, 201, 202, 203, 204:
		return nil
	case 400:
		return &BadRequestError{&HttpRequestError{400, "Bad request", "Bad request"}, fmt.Sprint(response.Request)}
	case 401:
		return UnauthorizedError{&HttpRequestError{401, "Unauthorized", "Unauthorized"}}
	case 403:
		return ForbiddenError{&HttpRequestError{403, "Forbidden", "Forbidden"}}
	case 404:
		return NotFound{&HttpRequestError{404, "Not Found", "Not Found"}}
	case 409:
		return ResourceAlreadyExists{&HttpRequestError{409, "Resource already exists", "Resource already exists"}}
	case 500:
		return InternalServerError{&HttpRequestError{500, "Internal server error", "Internal server error"}}
	}

	return &UnsupportedError{Message: fmt.Sprintf("We don't support code: %v", response.StatusCode())}
}

// Convert ResourceGetResponse to FileInformer object. ResourceGetResponse we get from Yandex Disk (through Unmarshal)
func converResourceGetResponseToFileInfo(resource *ResourceGetResponse) FileInformer {
	fileInfo := YandexFileInfo{&DefaultFileInfo{}}

	fileInfo.name = resource.Name
	fileInfo.size = resource.Size
	fileInfo.hash = resource.Hash

	return fileInfo
}

func getUrl(url, path string, provider *YandexStorageProvider) (*LinkResponse, error) {
	responseAsBytes, err := provider.get(url, map[string]string{"path": path})
	if err != nil {
		return nil, err
	}

	resource := &LinkResponse{}
	if err := json.Unmarshal(responseAsBytes, resource); err != nil {
		return nil, err
	}

	return resource, nil
}