package mellody_storage

import "fmt"

// Error returns when key setting not found in configuration.
type ConfigurationKeyDoesNotExistsError struct {
	Message string
}

func (e ConfigurationKeyDoesNotExistsError) Error() string {
	return e.Message
}

func raiseErrorKeyDoesNotFound(keyName string) error {
	return ConfigurationKeyDoesNotExistsError{fmt.Sprintf("Key '%v' was not founded", keyName)}
}