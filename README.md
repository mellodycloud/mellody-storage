# Мотивация #

Библиотека которая должна стать связующим элементом между всеми поддерживаемыми облачными хранилища. Через нее должны проходить все низкоуровневые операции с файлами в облаке.

# Хранилища #

Планируется реализовать поддержку следующих облачных провайдеров:

- [Microsoft Azure Store Blob](https://azure.microsoft.com/ru-ru/documentation/articles/storage-dotnet-how-to-use-blobs/)
- [OneDrive](https://onedrive.live.com/)
- [DropBox](https://www.dropbox.com/)
- [Google Drive](https://www.google.com/intl/ru_ru/drive/)
- [Yandex Disk](https://disk.yandex.ru/client/disk)
- [Vkontakte](http://vk.com)
- [WebDAV](https://ru.wikipedia.org/wiki/WebDAV)


```
#!go

Приоритет будет уточнен в ходе разработке.
```

# Архитектура #

Библиотека должна предоставлять пользователю фабрику хранилищ с едиными интерфейсом взаимодействия.

Интерфейс облачного хранилища должен предоставлять следующий набор операций:

- Авторизация в хранилище;
- Получение списка файлов из хранилища (структуры каталогов);
- Получение метаданных файла из хранилища (название, размер, дата загрузки)
- Загрузка файла в хранилище
- Извлечение файла из хранилища
- Удаление файла в хранилище
- Перемещение файла внутри хранилища
- Переименовывание файла в хранилище
- Проверка существования файла в хранилище

# Приступая к работе #

Для начала работы с проектом, выполните следующие шаги

* Скопируйте репозиторий на свой диск

```go
git clone https://**<ВАШ ЛОГИН>**@bitbucket.org/mellodycloud/mellody-project.git
```

* Создайте feature-бранч под свою задачу

```powershell
git checkout -b implement-azure-storage-backend-1 
```

либо из интерфейса bitbucket, тогда после нужно получить ветку с сервера

```powershell
git fetch && git checkout implement-azure-storage-backend-1

```

* После внесения своих изменений запустите и проверьте, что все тесты успешно проходят

```powershell
go.exe test -v ./...
```

* Отправьте изменения на сервер

``` powershell
git push origin implement-azure-storage-backend-1
```

* Создайте Pull Request

* После прохождения Code Review изменения будут слиты в develop

** !!! ВАЖНО !!! ** Если в процессе работы над веткой и после Code Review в ветке получилось больше чем один коммит, нужно провести операцию rebase и слить все комиты в один.

# Подготовка рабочего окружения #

* Для разработки библиотеки используется IntellyIDEA с плагином для поддержки Go. Но может быть использована или любая другая по предпочтению.
* Для запусков тестов, требуется установить переменные окружения

[**Azure**]
```
#!go

accountName:     mellodyblob
containerName:   user1
accountKey:     rDOl02IaOEBjwK7egCiy6nfn3bPLsTKewoM9t1leIk/zk3froRmJLPwMgRAXiOH3ZnCD9Nb+/f1RPSZc5Ecgag==

```

[**Yandex**]

```
#!go

ya-oauth-token:          AQAAAAADxjj4AANVKoIVxh2i50zKjx8UApeu6Vw
ya-application-id:       b47a59e7eaac4c008ec9fa8cb5aeae7c
ya-application-password: 67b65976e373464ab548d8cfefa6867c
```

