package mellody_storage

import (
	"github.com/Azure/azure-sdk-for-go/storage"
	"io"
	"bytes"
)

const (
	AZURE_ACCOUNT_KEY = "accountKey"
	AZURE_ACCOUNT_NAME = "accountName"

	AZURE_CONTAINER_NAME = "containerName"
)


// AzureStorageProvider implements StorageProvider interface for Azure Blob Storage
type AzureStorageProvider struct {
	Container     string
	StorageClient *storage.BlobStorageClient
}

// NewAzureClient constructs new AzureStorageProvider object.
func NewAzureClient(configuration map[string]string) (*AzureStorageProvider, error) {
	containerName, isContainerNameKeyExists := configuration[AZURE_CONTAINER_NAME]
	if !isContainerNameKeyExists {
		return nil, raiseErrorKeyDoesNotFound(AZURE_CONTAINER_NAME)
	}

	storage := &AzureStorageProvider{containerName, nil}
	storage.Container = containerName

	var err error = nil
	if storage.StorageClient, err = getBlobStorageClient(configuration); err != nil {
		return storage, err
	}

	return storage, nil
}

// AzureFileInfo implements FileInformer interface. It stores information file about.
type AzureFileInfo struct {
	*DefaultFileInfo
}

// CreateContainer creates folder or container where user will upload your files
func (provider *AzureStorageProvider) CreateContainer(name string) error {
	return provider.StorageClient.CreateContainer(name, storage.ContainerAccessTypeBlob)
}

// Deletes container and all files which stored there
func (provider *AzureStorageProvider) DeleteContainer(name string) error {
	return provider.StorageClient.DeleteContainer(name)
}

// ExistsContainer checks that container is exists
func (provider *AzureStorageProvider) ExistsContainer(name string) (bool, error) {
	return provider.StorageClient.ContainerExists(name)
}

// List gathers information about all files from storage.
func (provider *AzureStorageProvider) List() ([]FileInformer, error) {
	listOfBlobs, err := provider.StorageClient.ListBlobs(provider.Container, storage.ListBlobsParameters{})
	if err != nil {
		//ToDo: Need to lead all error to one standard. Now I throw specific error from Azure SDK.
		return nil, err
	}

	files := make([]FileInformer, len(listOfBlobs.Blobs))
	for idx, blob := range listOfBlobs.Blobs {
		files[idx] = newFileInfo(blob.Name, blob.Properties)
	}

	return files, nil
}

// Get retrieves information about file from storage.
func (provider *AzureStorageProvider) Get(path string) (FileInformer, error) {
	blobProps, err := provider.StorageClient.GetBlobProperties(provider.Container, path)
	if err != nil {
		return nil, err
	}

	return newFileInfo(path, *blobProps), nil
}

// Download loads file from storage
func (provider *AzureStorageProvider) Download(path string) ([]byte, error) {
	blob, err := provider.StorageClient.GetBlob(provider.Container, path)
	if err != nil {
		return nil, err
	}

	extractedFile := make([]byte, 0)
	for {
		buf := make([]byte, 1024)
		_, fileReadError := blob.Read(buf)

		if fileReadError != nil && fileReadError != io.EOF {
			return nil, fileReadError
		}

		// remove all null characters from buffer:
		clearBuf := bytes.Trim(buf, "\x00")
		extractedFile = append(extractedFile, clearBuf...)

		if fileReadError == io.EOF {
			break
		}
	}

	return extractedFile, nil
}

// Upload loads file to storage
func (provider *AzureStorageProvider) Upload(path string, content []byte) error {
	byteReader := bytes.NewReader(content)

	return provider.StorageClient.CreateBlockBlobFromReader(provider.Container, path, uint64(len(content)), byteReader, nil)
}

// GetUrl returns url to file download
func (provider *AzureStorageProvider) GetUrl(path string) string {
	return provider.StorageClient.GetBlobURL(provider.Container, path)
}

// Exists returns true when file is exists, otherwise returns false.
func (provider *AzureStorageProvider) Exists(path string) (bool, error) {
	return provider.StorageClient.BlobExists(provider.Container, path)
}

// Delete blob from Azure's container
func (provider *AzureStorageProvider) Delete(path string) (bool, error) {
	return provider.StorageClient.DeleteBlobIfExists(provider.Container, path, make(map[string]string))
}

// newFileInfo makes a new instance AzureFileInfo from BlobProperties object (from Azure SDK).
func newFileInfo(fileName string, blobProps storage.BlobProperties) FileInformer {
	return &AzureFileInfo{&DefaultFileInfo{fileName,
		blobProps.ContentLength,
		blobProps.ContentMD5, ""}}
}

func getBlobStorageClient(security_data map[string]string) (*storage.BlobStorageClient, error) {
	accountName, isAccountNameKeyExists := security_data[AZURE_ACCOUNT_NAME]
	if !isAccountNameKeyExists {
		return nil, raiseErrorKeyDoesNotFound(AZURE_ACCOUNT_NAME)
	}

	accountKey, isAccountKeyKeyExists := security_data[AZURE_ACCOUNT_KEY]
	if !isAccountKeyKeyExists {
		return nil, raiseErrorKeyDoesNotFound(AZURE_ACCOUNT_KEY)
	}

	return makeBlobStorageClient(accountName, accountKey), nil
}

func makeBlobStorageClient(accountName, accountKey string) *storage.BlobStorageClient {
	basicClient, _ := storage.NewBasicClient(accountName, accountKey)
	storageClient := basicClient.GetBlobService()

	return &storageClient
}