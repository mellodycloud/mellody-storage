package mellody_storage

import "fmt"

// Code: 401
type RequiredAuthorizationError struct {
	EngineName string
}

func (e *RequiredAuthorizationError) Error() string {
	return fmt.Sprintf("[%v] Required to pass authorization.", e.EngineName)
}

type HttpRequestError struct {
	Code        int
	Message     string
	Description string
}

func (e *HttpRequestError) Error() string {
	return fmt.Sprintf("%v Fail: %v (%v)", e.Code, e.Message, e.Description)
}

// Code: 400
type BadRequestError struct {
	*HttpRequestError
	Request string
}

func (e *BadRequestError) Error() string {
	return fmt.Sprintf("%v Fail: %v (%v)\nRequest: %v", e.Code, e.Message, e.Description, e.Request)
}

// Code: 401
type UnauthorizedError struct {
	*HttpRequestError
}

// Code: 403
type ForbiddenError struct {
	*HttpRequestError
}

// Code: 404
type NotFound struct {
	*HttpRequestError
}

// Code: 409
type ResourceAlreadyExists struct {
	*HttpRequestError
}

// Code: 500
type InternalServerError struct {
	*HttpRequestError
}

type UnsupportedError struct {
	Message    string
	InnerError *error
}

func (e *UnsupportedError) Error() string {
	return fmt.Sprintf("UnsupportedError. %v\n\t%v", e.Message, e.InnerError)
}